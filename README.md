# These tutorial are from [Go Cookbook](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwijo7D22KzjAhXS7HMBHYmrCfEQFjAAegQIAxAC&url=https%3A%2F%2Flibrary.kre.dp.ua%2FBooks%2F2-4%2520kurs%2F%25D0%259F%25D1%2580%25D0%25BE%25D0%25B3%25D1%2580%25D0%25B0%25D0%25BC%25D1%2583%25D0%25B2%25D0%25B0%25D0%25BD%25D0%25BD%25D1%258F%2520%252B%2520%25D0%25BC%25D0%25BE%25D0%25B2%25D0%25B8%2520%25D0%25BF%25D1%2580%25D0%25BE%25D0%25B3%25D1%2580%25D0%25B0%25D0%25BC%25D1%2583%25D0%25B2%25D0%25B0%25D0%25BD%25D0%25BD%25D1%258F%2FGo%2520Web%2Fgo-cookbook%2540bzd_channel.pdf&usg=AOvVaw3U2FVEqwJOTBES7D8BWiE2)
## **Chapter 4**: Error Handling in Go  
### basicerrors: Handling errors and the Error interface
How to define new errors?  
There are three ways:
1. using errors.New(aString) syntax and import "errors"
```
err := errors.New("this is a quick and easy way to create an error")
```
2. using fmt.Errorf(aStringFormat) syntax and improt "fmt"
```
err = fmt.Errorf("an error occured: %s", "something")
```
3. by create a new variable contains errors.New(aString)
```
var ErrorTyped = errors.New("this is a typed error")
err = ErrorTyped
```
4. by create a new type of Error that implement error interface
```
type CustomError struct {
	Result string
}

func (c CustomError) Error() string {
	return fmt.Sprintf("there was an error; %s was the result", c.Result)
}

// SomeFunc returns an error
func SomeFunc() error {
	c := CustomError{Result: "this"} //this is how to create instance in Golang
	return c
}
```

### errwrap: Using the pkg/errors package and wrapping errors
*Wrapping errors* is like classify errors into a new category. It has got standarized printed error, like "{wrap-error syntax}: {specific error syntax}"
*Unwrapping errors* is break down wrapped error, so we can assert it.
*StackTrace* is to print all the error message include the default error message

### panic: Catching panics for long running processes
*defer* is literal that use to define that the function will execute before the function returns. usually used to an IO operation.  
*recover* is method that catch panics and willn't exited all the program, only terminated the error function, and will back to the function that called it.
```
func Panic() {
	zero, err := strconv.ParseInt("0", 10, 64)
	if err != nil {
		panic(err)
	}
	a := 1 / zero
	fmt.Println("we'll never get here", a)
}

// Catcher calls Panic
func Catcher() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("panic occured:", r)
		}
	}()
	Panic()
}
```

## Chapter 5: All about Databases and Storage  
### database: The database/sql package with MySQL
```sql.Open("mysql","{username}:{password}@/{db_name}?parseTime=true"``` will connect to your database.  
```*sql.DB.Exec("{any sql statement}")``` is to execute sql query.  
example to query:  
```
func Query(db *sql.DB) error {
	name := "Aaron"
	rows, err := db.Query("SELECT name, created FROM example where name=?", name)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var e Example
		if err := rows.Scan(&e.Name, &e.Created); err != nil {
			return err
		}
		fmt.Printf("Results:\n\tName: %s\n\tCreated: %v\n",
			e.Name, e.Created)
	}
	return rows.Err()
}
```

## Chapter 9: Parallelism and Concurrency  

# These tutorial are from [Dasar Pemrograman Golang - Noval Agung](https://dasarpemrogramangolang.novalagung.com/B-1-golang-web-hello-world.html)
**webapp**: B1. Golang Web App: Hello World